import Vue from 'vue'
import { Icon, Swipe, Toast, SwipeItem, Notify, Search, Popup, ImagePreview, Loading, Field, Image, Overlay, Checkbox, Button, Dialog, NavBar, Tab, Tabs, List, PullRefresh, Cell, Divider, Lazyload } from 'vant'

Vue.use(Icon)
Vue.use(Overlay)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Popup)
Vue.use(Field)
Vue.use(Checkbox)
Vue.use(Button)
Vue.use(NavBar)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(PullRefresh)
Vue.use(List)
Vue.use(Cell)
Vue.use(Divider)
Vue.use(Image)
Vue.use(Loading)
Vue.use(Search)
Vue.use(Notify)
Vue.use(ImagePreview)
Vue.use(Lazyload)
Vue.prototype.$dialog = Dialog
Vue.prototype.$toast = Toast
Vue.prototype.$notify = Notify
Vue.prototype.$imagePreview = ImagePreview

import cmblapi from 'cmblapi'
export const formatDate = (date) => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-')
}
const formatNumber = (n) => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

export const functionmakeForm = (query, url) => {
  const form = document.createElement('form')
  form.id = 'form-file-download'
  form.name = 'form-file-download'
  // 添加到 body 中
  document.body.appendChild(form)
  for (const key in query) {
    if (query[key] !== undefined && Object.hasOwnProperty.call(query, key)) {
      // 创建一个输入
      const input = document.createElement('input')
      input.type = 'hidden'
      input.name = key
      input.value = query[key]
      form.appendChild(input)
    }
  }
  // form 的提交方式
  form.method = 'POST'
  form.formenctype = 'application/x-www-form-urlencoded'
  //跳转新页面
  form.target = '_blank'
  // form 提交路径
  form.action = url
  form.submit()
  document.body.removeChild(form)
}

export const closeAppHandle = () => {
  cmblapi.applet({
    api: 'checkAppletContainer',
    success: function() {
      //如果是小程序容器，这里回调
      cmblapi.applet({
        api: 'popWindow',
        fail: function(res) {},
      })
    },
    fail: function(res) {
      //如果不是小程序容器，这里回调
    },
  })
}

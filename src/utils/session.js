export function setInfo(val) {
  sessionStorage.setItem('info', JSON.stringify(val))
}
export function getInfo() {
  return JSON.parse(sessionStorage.getItem('info'))
}
export function removeInfo() {
  sessionStorage.removeItem('info')
}
export function setCity(val) {
  sessionStorage.setItem('city', val)
}
export function getCity() {
  return sessionStorage.getItem('city')
}
export function removeCity() {
  sessionStorage.removeItem('city')
}

export function setHomeData(val) {
  sessionStorage.setItem('homeData', JSON.stringify(val))
}
export function getHomeData() {
  return JSON.parse(sessionStorage.getItem('homeData'))
}
export function removeHomeData() {
  sessionStorage.removeItem('homeData')
}
// 保存新手引导的操作
export function setGuideTask(val) {
  sessionStorage.setItem('guideTask', JSON.stringify(val))
}
export function getGuideTask() {
  return JSON.parse(sessionStorage.getItem('guideTask'))
}
export function removeGuideTask() {
  sessionStorage.removeItem('guideTask')
}

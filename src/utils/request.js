import axios from 'axios'
import { Toast } from 'vant'
// 创建实例
const server = axios.create({
  baseURL: process.env.VUE_APP_API, // 路径
  timeout: 5000,
})

// 请求拦截
server.interceptors.request.use(
  (config) => {
    // 请求成功
    config.headers['x-auth-token'] = '132132131'
    return config
  },
  (error) => {
    // 请求失败
    return Promise.reject(error)
  }
)

// 响应拦截
server.interceptors.response.use(
  (response) => {
    // 请求成功
    const { data } = response
    return data
  },
  (error) => {
    const { response } = error
    if (!response) {
      Toast.fail(`网络连接失败`)
    }
    return Promise.reject(error)
  }
)
export default server

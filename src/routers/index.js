import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// 解决重复点击路由报错的BUG
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}
const routesArr = [
  {
    path: '/',
    component: () => import('../views/index.vue'),
  },
  {
    path: '/cancelIndex',
    name: 'CancelIndex',
    component: () => import('../views/cancelIndex.vue'),
  },
  {
    path: '/comfirmIndex',
    name: 'ComfirmIndex',
    component: () => import('../views/comfireIndex.vue'),
  },
  {
    path: '/searchIndex',
    name: 'SearchIndex',
    component: () => import('../views/searchIndex.vue'),
  },
]

const router = new VueRouter({
  base: '/app',
  routes: routesArr,
})
router.beforeEach((to, from, next) => {
  next()
})
export default router

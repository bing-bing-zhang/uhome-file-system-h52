import server from '@/utils/request'

// 获取会员余额支付码
export function saveLogs(data = {}) {
    return server.request({
        url: `/api/h5/query/storeLog`,
        method: 'GET',
    })
}

//商品搜索日志
export function saveProductLogs(data = {}) {
    return server.request({
        url: `/api/h5/query/productLog`,
        method: 'GET',
        params: data
    })
}

//门店搜索日志
export function saveStoreLogs(data = {}) {
    return server.request({
        url: `/api/h5/query/storeLog`,
        method: 'GET',
        params: data
    })
}

//错误信息上报
export function saveReportErrLogs(data = {}) {
    return server.request({
        url: `/api/h5/message/reportErr`,
        method: 'POST',
        data: data
    })
}

import server from '@/utils/request'

// 搜索店铺
export function searchShopApi(data = {}) {
  return server.request({
    url: `/api/h5/query_store_data`,
    method: 'GET',
    params: data,
  })
}
// 搜索商品
export function searchGoodApi(data = {}) {
  return server.request({
    url: `/api/h5/query_product_data`,
    method: 'GET',
    params: data,
  })
}

// 登录
export function h5Login(data = {}) {
  return server.request({
    url: `/api/pc/h5/qrLogin`,
    method: 'GET',
    params: data,
  })
}

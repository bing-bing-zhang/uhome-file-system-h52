import Vue from 'vue'
import 'amfe-flexible'
import App from './App.vue'
import router from '@/routers'
import './utils/vantConfig'
import './assets/styles/main.scss' //引入scss
import './assets/iconfont/iconfont.css'
import store from './store'
import VueWechatTitle from 'vue-wechat-title'
Vue.config.productionTip = false
Vue.prototype.$baseImgUrl = process.env.VUE_APP_BASE_IMAGE_URL

Vue.use(VueWechatTitle)
new Vue({
  render: (h) => h(App),
  store,
  router,
}).$mount('#app')

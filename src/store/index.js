import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    city: '',
    username: '',
    zsAgreementCodeStatusState: 0,
    firstRechargeStatus: false, //控制首充弹窗的次数
    guideTaskStatus: false, //控制新手引导的此时
    requestInfoData: {},
  },
  mutations: {
    changeCity(state, city) {
      state.city = city
    },
    changeUserName(state, username) {
      state.username = username
    },
    saveZsAgreementCodeStatusHandle(state, data) {
      state.zsAgreementCodeStatusState = data
    },
    saveRequestInfoDataHandle(state, data) {
      state.requestInfoData = data
    },
    changeFirstRechargeStatus(state, data) {
      state.firstRechargeStatus = data
    },
    changeGuideTaskStatus(state, data) {
      state.guideTaskStatus = data
    },
  },
  actions: {},
  modules: {},
})
